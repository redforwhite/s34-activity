
const express = require('express')

const world = express()

const users = [{
		username: "johndoe",
		password: "johndoe69"
	}];

const port = 3000

world.use(express.json())
world.use(express.urlencoded({extended:true}))

world.get('/home', (request, response) => {
	response.send('Welcome to homepage')
})

world.get('/user', (request, response) => {
	response.send(users)
})

world.delete('/delete-item', (request, response) => {
  
  const deleteUser = users.findIndex(user => user.username === request.body.username);
  if (deleteUser !== -1) {
    users.splice(deleteUser, 1);
    response.send(`User has been deleted`);
  } else {
    response.send(`User not found`);
  }
});


world.listen(port, () => console.log(`Server is running at localhost:${port}`))